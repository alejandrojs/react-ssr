const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  devtool: 'source-map',
  entry: ['webpack-hot-middleware/client', './client/index.js'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new HtmlWebpackPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
      'react-native$': 'react-native-web/dist/cjs'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /\/node_modules\/(?!react-router-native)/,
        include: __dirname,
        options: {
          cacheDirectory: false,
          presets: ['module:metro-react-native-babel-preset'],
          plugins: [
            ['react-native-web', { commonjs: true }],
            // needed to support async/await
            '@babel/plugin-transform-runtime'
          ]
        }
      }
    ]
  }
};
