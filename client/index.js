// @flow
import '@babel/polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import configureStore, { history } from '../common/store';
import App from '../common';

const preloadedState = window.__PRELOADED_STATE__;
const store = configureStore(preloadedState, history());
const rootElement = window.document.getElementById('app');

console.log('--------- CLIENT ---------');
console.log(preloadedState);
console.log('--------------------------');

render(
  <Provider store={store}>
    <ConnectedRouter history={history()}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ConnectedRouter>
  </Provider>,
  rootElement
);

if (module.hot) {
  module.hot.accept();
}
