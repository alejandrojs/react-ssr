// const path = require('path');

require('@babel/register')({
  cache: false,
  ignore: [/\/node_modules\/(?!react-router-native)/]
});

require('./app');
