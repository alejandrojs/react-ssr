import Express from 'express';
import qs from 'qs';

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config';

import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { Provider } from 'react-redux';

import { AppRegistry } from 'react-native-web';
import { StaticRouter } from 'react-router';

import configureStore, { history } from '../common/store';
import App from '../App';
import { fetchCounter } from '../common/api/counter';

const app = new Express();
const port = 3000;

AppRegistry.registerComponent('App', () => App);

// Use this middleware to set up hot module reloading via webpack.
const compiler = webpack(webpackConfig);
app.use(
  webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath })
);
app.use(webpackHotMiddleware(compiler));

const handleRender = (req, res) => {
  // TODO: initialProps con initialURL sacada de req?
  const initialProps = {};
  const { element, getStyleElement } = AppRegistry.getApplication('App', { initialProps });
  const css = renderToStaticMarkup(getStyleElement({ nonce: 'style-app' }));

  console.log('--------- HANDLE RENDER ---------');
  console.log(`${req.method} ${req.originalUrl}`);

  // Query our mock API asynchronously
  fetchCounter(apiResult => {
    // Read the counter from the request, if provided
    const params = qs.parse(req.query);
    const counter = parseInt(params.counter, 10) || apiResult || 0;

    console.log(`--------- FETCH COUNTER ${counter} ---------`);

    // Compile an initial state
    const preloadedState = { counter };

    // Create a new Redux store instance
    const store = configureStore(preloadedState, history({ initialEntries: [req.url] }));

    console.log('req.url ', req.url);

    // Render the component to a string
    const html = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={{}}>
          {element}
        </StaticRouter>
      </Provider>
    );

    // Grab the initial state from our Redux store
    const finalState = store.getState();

    // Send the rendered page back to the client
    res.send(renderFullPage(html, css, finalState));
  });
};

// This is fired every time the server side receives a request
app.use(handleRender);

const renderFullPage = (html, css, preloadedState) => {
  return `
    <!doctype html>
    <html>
      <head>
        <title>Redux Universal Example</title>
      </head>
      ${css}
      <body>
        <div id="app">${html}</div>
        <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\x3c')}
        </script>
        <script nonce="style-app" src="/static/bundle.js"></script>
      </body>
    </html>
    `;
};

app.listen(port, error => {
  if (error) {
    console.error(error);
  } else {
    console.info(
      `==> 🌎  Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`
    );
  }
});
