import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import Stack from 'react-router-native-stack';
import * as CounterActions from './actions';

import routes from './routes';

class App extends React.Component {
  render() {
    return <Stack>{renderRoutes(routes)}</Stack>;
  }
}

const mapStateToProps = state => ({
  counter: state.counter
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(CounterActions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
