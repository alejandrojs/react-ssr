import * as React from 'react';
import PropTypes from 'prop-types';
import { Button, View, Text } from 'react-native';

class Counter extends React.PureComponent {
  render() {
    const { increment, decrement, counter } = this.props;
    return (
      <View style={{ flexDirection: 'row' }}>
        <Text>Clicked: {counter} times </Text>
        <Button title="+" onPress={increment} />
        <Button title="-" onPress={decrement} />
      </View>
    );
  }
}

Counter.propTypes = {
  increment: PropTypes.func.isRequired,
  incrementIfOdd: PropTypes.func.isRequired,
  incrementAsync: PropTypes.func.isRequired,
  decrement: PropTypes.func.isRequired,
  counter: PropTypes.number.isRequired
};

export default Counter;
