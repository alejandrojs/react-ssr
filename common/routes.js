// @flow

import { default as ModuleA } from './modules/a';
import { default as ModuleB } from './modules/b';

const routes = [
  {
    path: '/',
    exact: true,
    component: ModuleA
  },
  {
    path: '/b',
    exact: true,
    component: ModuleB
  }
];

export default routes;
