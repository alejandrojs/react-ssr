// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { type History } from 'history';

import counter from './counter';

const createReducers = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    counter
  });

export default createReducers;
