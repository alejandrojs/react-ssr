import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import history from './history';

const middleware = [thunk, routerMiddleware(history)];

const enhancer = composeWithDevTools(applyMiddleware(...middleware));

export { createStore, enhancer };
