import { createStore, enhancer } from './config';
import history from './history';

import createReducers from '../reducers';

const configureStore = (preloadedState, history) => {
  const store = createStore(createReducers(history), preloadedState, enhancer);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export { configureStore as default, history };
