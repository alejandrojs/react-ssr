// @flow
import { type Location } from 'react-router';

export const locationSelector = (state: any): Location => state.router.location;
