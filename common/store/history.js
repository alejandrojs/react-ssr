// @flow
import { createBrowserHistory, createMemoryHistory } from 'history';

const isClient = () => {
  try {
    return window?.document?.createElement;
  } catch (e) {
    return false;
  }
};

const history = isClient() ? createBrowserHistory : createMemoryHistory;

export default history;
