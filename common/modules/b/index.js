// @flow
import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';

type Props = {
  goBack: Function
};

class ModuleB extends React.Component<Props> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text>Module B</Text>
        <Button onPress={this.props.goBack} title="back" />
      </View>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      goBack
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModuleB);
