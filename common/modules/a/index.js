// @flow
import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

type Props = {
  push: Function
};

class ModuleA extends React.Component<Props> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text>Module A</Text>
        <Button onPress={() => this.props.push('/b')} title="Go To Page B" />
      </View>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      push
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModuleA);
