console.log('babel.config');

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    '@babel/plugin-transform-modules-commonjs',
    '@babel/plugin-transform-runtime',
    // 'react-native-web',
    [
      'react-native-web',
      {
        commonjs: true
      }
    ],
    // 'add-module-exports',
    'react-hot-loader/babel'
  ]
};
